module gitee.com/kzangv/gsf-fof-es

go 1.18

require (
	gitee.com/kzangv/gsf-fof v0.5.2
	github.com/elastic/go-elasticsearch/v8 v8.2.0
	github.com/urfave/cli/v2 v2.24.1
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/elastic/elastic-transport-go/v8 v8.1.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
)
