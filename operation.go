package es

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/kzangv/gsf-fof/logger"
	"net/http"
	"time"
)

func OperationApiLog(c *Client, op, idx, body string) func() {
	bTm := time.Now()
	return func() {
		spendTime := time.Now().Sub(bTm) / time.Millisecond
		if spendTime > time.Duration(c.ref.Cfg.SlowThreshold) { // 查询超过阈值
			c.Log.Warn(" ES[%d ms]: %s/%s -- %s", spendTime, idx, op, body)
		} else if c.Log.Level() >= logger.Info {
			c.Log.Info(" ES[%d ms]: %s/%s -- %s", spendTime, idx, op, body)
		}
	}
}

type Operation struct {
	val    *Client
	ApiLog func(c *Client, op, idx, body string) func()
}

func NewOperation(c *Client) *Operation {
	return &Operation{val: c, ApiLog: OperationApiLog}
}

func (c *Operation) Search(idx string, body string, receive interface{}, ctx context.Context) (cnt uint64, err error) {
	reqData := &bytes.Buffer{}
	reqData.WriteString(body)

	defer c.ApiLog(c.val, "Search", idx, body)()

	resp, err := c.val.Client.Search(c.val.Client.Search.WithIndex(idx), c.val.Client.Search.WithBody(reqData), c.val.Client.Search.WithContext(ctx))

	if err == nil {
		defer resp.Body.Close()
		if resp.StatusCode == http.StatusOK {
			data := RespSearch{}
			data.Hits.Hits = receive
			if err = json.NewDecoder(resp.Body).Decode(&data); err == nil {
				cnt = data.Hits.Total.Value
			}
		} else {
			raw := RespError{}
			if err = json.NewDecoder(resp.Body).Decode(&raw); err == nil {
				err = fmt.Errorf(raw.Error.Reason)
			}
		}
	}

	return
}
